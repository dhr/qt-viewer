PYTHON = /usr/bin/env python

mac:
	./create_ui_files
	${PYTHON} setup.py py2app
	./create_dmg
	rm -rf build .eggs dist

clean:
	rm -rf build .eggs dist MultiViewerMain.pyc cinema_*.dmg
