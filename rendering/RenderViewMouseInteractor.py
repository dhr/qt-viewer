from PySide import QtCore
from PySide.QtCore import QObject, Qt


class RenderViewMouseInteractor(QObject):
    """ base class for items that map mouse motions in a renderview
    into requests for images from a different viewpoint from the store """
    NoneState = 0
    RotateState = 1
    PanState = 2
    ZoomState = 3

    def __init__(self, parent=None):
        super(RenderViewMouseInteractor, self).__init__(parent)
        self._state = self.NoneState
        self._xy = None

        # Current camera settings
        self._scale = 1

    def getScale(self):
        return self._scale

    @QtCore.Slot('QMouseEvent')
    def onMousePress(self, mouseEvent):
        if (mouseEvent.button() == Qt.LeftButton):
            self._xy = (mouseEvent.x(), mouseEvent.y())
            self._state = self.RotateState
        elif (mouseEvent.button() == Qt.RightButton):
            self._state = self.ZoomState
            self._xy = (mouseEvent.x(), mouseEvent.y())

    @QtCore.Slot('QMouseEvent')
    def onMouseMove(self, mouseEvent):
        if (self._xy is None):
            return

        dx = mouseEvent.x() - self._xy[0]
        dy = mouseEvent.y() - self._xy[1]

        if (self._state == self.RotateState):
            self.mouseMove(mouseEvent.x(), mouseEvent.y(), dx, dy)

        elif (self._state == self.ZoomState):
            scaleFactor = 1.01
            if (dy < 0):
                for i in range(-dy):
                    self._scale = self._scale * scaleFactor
            else:
                for i in range(dy):
                    self._scale = self._scale * (1.0 / scaleFactor)
            self._xy = (mouseEvent.x(), mouseEvent.y())

    @QtCore.Slot('QMouseEvent')
    def onMouseRelease(self, mouseEvent):
        self._xy = None

    @QtCore.Slot('QWheelEvent')
    def onMouseWheel(self, event):
        scaleFactor = 1.01
        # reduce the size of delta for more controllable zooming
        dy = event.delta() / 20
        if (dy > 0):
            for i in range(dy):
                self._scale = self._scale * scaleFactor
        else:
            for i in range(-dy):
                self._scale = self._scale * (1.0 / scaleFactor)

    def updateCamera(self):
        # a chance to fire off final events
        print "WARNING something is wrong child class should be running"
        pass
