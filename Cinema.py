#!/usr/bin/env python

#  @description Cinema viewer application. Supports loading multiple databases
#  simultaneously. Either single or a multi-database directories can be loaded
#  directly from the file menu or by passing the top info.json as an argument.
#
#  - Single database
#  Pass the database's info.json path as an argument.
#
#  - Multiple databases
#  Pass the top-directory's info.json path as an argument. The top-directoy
#  info.json is expected to have the following format:
#
#  {
#     "metadata": {
#         "type": "workbench"
#     },
#     "runs": [
#         {
#         "title": "image_0",
#         "description": "image_0",
#         "path": "image_0"
#         },
#         {
#         "title": "image_1",
#         "description": "image_1",
#         "path": "image_1"
#         }
#     ]
# }
#
# Where "path" is the name of the directory containing the info.json of a
# particular database.

import os
import argparse
import textwrap
import sys
from PySide.QtGui import QApplication
from MultiViewerMain import ViewerMain
from common import DatabaseLoader

# ---------------------------------------------------------------------
#
# adjust path, depending upon use case. This fixes a bug for python
#     applications that makes it crash, due to access permission for
#     file open/close
#
# ---------------------------------------------------------------------
curpath = os.getcwd()
if not curpath.startswith('/Users'):
    os.chdir(os.environ['HOME'])

# ---------------------------------------------------------------------
#
# application global variables
#
# ---------------------------------------------------------------------
APP_VERSION = "1.0"
APP_NOTDEFINED = "NOTDEFINED"


# ---------------------------------------------------------------------
#
# parse command line args
#
# ---------------------------------------------------------------------
conf_parser = argparse.ArgumentParser(
    # Turn off help, so we print all options in response to -h
    add_help=False
    )
args, remaining_argv = conf_parser.parse_known_args()

# ---------------------------------------------------------------------------
#
# command line options
#
# ---------------------------------------------------------------------------
# Don't surpress add_help here so it will handle -h
parser = argparse.ArgumentParser(
    # Don't mess with format of description
    formatter_class=argparse.RawDescriptionHelpFormatter,
    # Inherit options from config_parser
    parents=[conf_parser],
    # print script description with -h/--help
    epilog=textwrap.dedent('''\
        examples:

          python Cinema.py (no args)
            open the application and allow the user to open a file
            through dialogue boxes.
            ''')
)
parser.add_argument("-v", "--verbose", action="store_true", default=False,
                    help="report verbosely")
parser.add_argument("--version", action="version", version=APP_VERSION)
parser.add_argument("--database", default=APP_NOTDEFINED,
                    help="open this file when application starts")
args = parser.parse_args(remaining_argv)


# ---------------------------------------------------------------------
# create the qt app
a = QApplication(sys.argv)
v = ViewerMain()

# ---------------------------------------------------------------------
#
# handle args
#
# ---------------------------------------------------------------------
if not (args.database == APP_NOTDEFINED):
    databases, links = DatabaseLoader.loadAll(args.database)
    v.setStores(databases, links)

# ------------------- start profiling -------------------------------
# import cProfile, pstats, StringIO
# pr = cProfile.Profile()
# pr.enable()
# -------------------------------------------------------------------

v.show()
qAppRet = a.exec_()

# --------------------- finish profiling ----------------------------
# pr.disable()
# s = StringIO.StringIO()
# sortby = 'cumulative'
# ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
# ps.print_stats(0.1) # print 10% of calls
# print s.getvalue()
# -------------------------------------------------------------------

sys.exit(qAppRet)
