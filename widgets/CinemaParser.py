import abc

from PipelineItem import PipelineItem


class CinemaParser(object):
    __metaclass__ = abc.ABCMeta
    ''' Parses a cinema database depending on its specification. Abstract
    class, derive from this class to implement concrete cinema specs.'''
    def __init__(self):
        return

    @abc.abstractmethod
    def parse(self, store, names, root, associations={}):
        return

    @abc.abstractmethod
    def initializeQuery(self, rootItem):
        return

    @abc.abstractmethod
    def __addItem(self, paramName, parameter_list, prefix="", suffixDisplay="",
                  rootItem=None):
        return

    @abc.abstractmethod
    def __isControlValue(self, valueName, parameter):
        return


class CinemaParser_SpecA(CinemaParser):
    ''' Original loader of Viewer2.0.  Loads all the pipeline items without any
    hierarchy. Compatible with old SpecA datasets (and perhaps some
    old SpecB). '''
    def __init__(self):
        super(CinemaParser_SpecA, self).__init__()

    def parse(self, store, names, root, associations={}):
        '''Associations is unused. '''
        parameter_list = store.parameter_list
        for n in names:
            # skip altogether if named 'layer' (control var, not to show)
            if not n.startswith('layer'):

                if n.startswith('color'):
                    trimmedName = n[5:]  # trim 'color'

                    # only include a 'color' when its actual parameter is not
                    # otherwise available (e.g. calculator) OR it contains
                    # value arrays not only control variables (necessary for
                    # filters with values and value arrays)
                    if (trimmedName not in names) or \
                       ("value" in parameter_list[n]["types"]):
                        self._CinemaParser__addItem(trimmedName,
                                                    parameter_list,
                                                    prefix="color",
                                                    suffixDisplay="_Arrays",
                                                    rootItem=root)
                # actual user-defined parameters (e.g. clip, filter,  etc.)
                else:
                    self._CinemaParser__addItem(n, parameter_list,
                                                rootItem=root,
                                                radioButton=True)

        return

    def initializeQuery(self, rootItem):
        query = dict()
        rootChildren = rootItem._childItems
        for i in rootChildren:

            s = set()
            for j in i._childItems:

                if j.isVisible():
                    # query the actual name in store.parameters_list
                    s.add(j._dbParameterName)

            if len(s) > 0:  # only add layer/field if any child is visible
                query[i._dbParameterName] = s

        return query

    def _CinemaParser__addItem(self, paramName, parameter_list, prefix="",
                               suffixDisplay="", rootItem=None,
                               radioButton=False):
        ''' Appends an item and its user-expected values (as children) to the
        model. An optional prefix can be passed for parameters specified with a
        prefix in the database (e.g. 'Calculator' with
        'color' -> 'colorCalculator'). This method is NOT intended to add items
        to the model dynamically (would not update an AbstractView
        appropriately), as it does not make necessary AbstractModel calls.'''

        # add parameter as item
        paramNameInDb = prefix + paramName

        parameter = parameter_list[paramNameInDb]
        displayName = parameter["label"] + suffixDisplay
        # empty string disables visibility selection and properties in item
        # (for root parameters)
        parentItem = PipelineItem([displayName, ''])

        if displayName != paramNameInDb:
            parentItem.setParameterName(paramNameInDb)

        rootItem.appendChild(parentItem)

        ranges = {}
        if 'valueRanges' in parameter:
            ranges = parameter['valueRanges']

        # add its user-meaningful values as children items
        defaultValue = None
        if 'default' in parameter:
            defaultValue = parameter['default']

        for v in parameter['values']:
            # skip it if v si a field buffer type (control types as depth or
            # luminance)
            if self._CinemaParser__isControlValue(v, parameter):
                continue

            r = tuple([None, None])
            if v in ranges:
                r = ranges[v]

            # set the defined default as visible
            if v != defaultValue:
                childItem = PipelineItem([v, 'No'], parentItem)
            else:
                childItem = PipelineItem([v, 'Yes'], parentItem)
                childItem._visibility = True

            childItem.propertyItem().setValueRange(r)
            childItem._radioButtonBehavior = radioButton
            parentItem.appendChild(childItem)

        return

    def _CinemaParser__isControlValue(self, valueName, parameter):
        ''' Identifies a value as a control value or an actual user-meaningful
        value '''
        if 'isfield' in parameter and parameter['isfield'] == 'yes':
            valueIndex = parameter['values'].index(valueName)
            valueType = parameter['types'][valueIndex]
            return (valueType == "luminance" or valueType == "depth")

        return False


class CinemaParser_SpecB(CinemaParser_SpecA):
    def __init__(self, defaultClut=None):
        super(CinemaParser_SpecB, self).__init__()
        self.__clut = defaultClut

    def parse(self, store, names, root, associations):
        parameters = store.parameter_list
        for visItem in parameters["vis"]["values"]:
            parentItem = self.__addVisItem(visItem, root)

            allParameters = store.parameters_for_object(visItem)

            # add related field
            for item in [allParameters[1]]:
                self._CinemaParser__addItem(item, parameters,
                                            suffixDisplay="_Arrays",
                                            rootItem=parentItem,
                                            radioButton=True)

            # add related controls (check the list of dependers to avoid
            # adding repeated controls)
            for item in allParameters[2]:
                if visItem in store.getdependers(item):
                    self._CinemaParser__addItem(item, parameters,
                                                suffixDisplay="_Control",
                                                rootItem=parentItem)

        return

    def initializeQuery(self, rootItem):
        query = dict()
        rootChildren = rootItem._childItems
        visSet = set()

        for vis in rootChildren:

            visChildren = vis._childItems

            # even if not visible, add its children as they might be dependees
            # of others
            if vis.isVisible():
                visSet.add(vis._dbParameterName)

            for i in visChildren:

                s = set()
                for j in i._childItems:

                    if j.isVisible():
                        # query the actual name in store.parameters_list
                        s.add(j._dbParameterName)

                if len(s) > 0:  # only add layer/field if any child is visible
                    query[i._dbParameterName] = s

        if len(visSet) > 0:
            query["vis"] = visSet

        return query

    def __addVisItem(self, visItemName, rootItem):
        ''' Adds a 'vis' layer item on the top layer. '''
        parentItem = PipelineItem([visItemName, "Yes"])
        parentItem._visibility = True
        # parentItem.setParameterName("vis/" + visItemName)
        parentItem.propertyItem().setColorLut(self.__clut)
        rootItem.appendChild(parentItem)
        return parentItem

    def _CinemaParser__isControlValue(self, valueName, parameter):
        ''' Identifies a value as a control value or an actual user-meaningful
        value '''
        if parameter["role"] == "field":
            valueIndex = parameter['values'].index(valueName)
            valueType = parameter['types'][valueIndex]
            return (valueType == "luminance" or valueType == "depth")

        return False
