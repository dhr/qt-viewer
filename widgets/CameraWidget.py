import PIL
from PySide import QtCore
from PySide.QtCore import Signal
from PySide.QtGui import QImage, QPixmap

from common.CinemaSpec import CinemaSpec
from widgets.DisplayWidget import DisplayWidget
import cinema_python.images.querymaker as querymaker
import cinema_python.images.querymaker_specb as querymaker_specb


class CameraWidget(DisplayWidget):
    ''' Widget which contains all the necessary instances to display a Cinema
    data-base.

    Requires a Cinema store to be set at initialization. It receives queries
    from ControllersWidget which are transformed into a series of LayerSpec
    instances (by means of a QueryMaker).'''

    cameraSelected = Signal(int)

    def __init__(self, parent):
        super(CameraWidget, self).__init__(parent)
        self.__translator = None
        self.__id = None
        self._ui._renderView.mousePressSignal.connect(self.__onPressSignal)

    @QtCore.Slot()
    def __onPressSignal(self):
        ''' Forwards a signal about its selection to the parent widget (emitted
        on a mouse press event).'''
        self.cameraSelected.emit(self.__id)

    def __render(self, layers, colorDefinitions):
        ''' Computes and displays a composited image.'''
        try:
            self.compositor.setColorDefinitions(colorDefinitions)
            compositeImage = self.compositor.render(layers)
        except IndexError:
            # Handles the case when there are no layers selected
            # (Compositor throws) or layers were not loaded correctly
            print "Warning: No Layers were selected or Layers failed to load!"
            self.renderView().clear()
            return

        # Pass the result to the viewport widget
        pimg = PIL.Image.fromarray(compositeImage)
        try:
            imageString = pimg.tostring('raw', 'RGB')
        except Exception:
            # Pillow for OS X has replaced "tostring" with "tobytes"
            imageString = pimg.tobytes('raw', 'RGB')

        qimg = QImage(imageString, pimg.size[0], pimg.size[1], pimg.size[0]*3,
                      QImage.Format_RGB888)
        pix = QPixmap.fromImage(qimg)

        # Try to resize the display widget
        self.renderView().sizeHint = pix.size
        self.renderView().setPixmap(pix)

    @QtCore.Slot()
    def update(self, query, colorDefinitions):
        ''' Translates a query and passes LayerSpecs to the compostior.'''
        layers = self.__translator.translateQuery(query)
        self.__render(layers, colorDefinitions)

    def setId(self, Id):
        self.__id = Id

    def getId(self):
        return self.__id

    def setStore(self, store, spec):
        ''' Initialization. '''
        if (spec is CinemaSpec.A):
            self.__translator = querymaker.QueryMaker_SpecA()
        elif (spec is CinemaSpec.B):
            self.__translator = querymaker_specb.QueryMaker_SpecB()
        super(CameraWidget, self).setSpecification(spec)

        self.__translator.setStore(store)
