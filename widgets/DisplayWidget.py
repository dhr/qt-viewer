from PySide import QtCore
from PySide.QtGui import QColor, QBrush, QFrame
from uiFiles import ui_DisplayWidget as ui
import cinema_python.images.compositor as compositor
from common.CinemaSpec import CinemaSpec


class DisplayWidget(QFrame):
    ''' Nexus for interaction events between the main application
    window the RenderView widget which shows the data and the interaction
    handling class which interprets mouse events according to the specific
    store type.'''

    def __init__(self, parent):
        super(DisplayWidget, self).__init__(parent)
        self._ui = ui.Ui_DisplayWidget()
        self._mouseInteractor = None
        self.__compositor = None
        self._ui.setupUi(self)

    def setSpecification(self, spec):
        if (spec is CinemaSpec.A):
            self.__compositor = compositor.Compositor_SpecA()
            bgcolor = QColor(0, 0, 0, 255)
        elif (spec is CinemaSpec.B):
            self.__compositor = compositor.Compositor_SpecB()
            bgcolor = QColor(80, 81, 109, 255)

        self.setBackgroundColor(bgcolor)

    def setInteractor(self, interactor):
        self.disconnectInteractorSignals()
        self._mouseInteractor = interactor
        self.connectInteractorSignals()
        interactor.setParent(self)

    def _updateCamera(self):
        if not self._mouseInteractor:
            print "Warning: No interactor set to update!"
            return

        scale = self._mouseInteractor.getScale()
        self._ui._renderView.resetTransform()
        self._ui._renderView.scale(scale, scale)
        self._mouseInteractor.updateCamera()

    def connectInteractorSignals(self):
        if not self._mouseInteractor:
            print "Warning: No interactor set to connect signals!"
            return

        rv = self._ui._renderView
        mi = self._mouseInteractor
        rv.mousePressSignal.connect(mi.onMousePress)
        rv.mouseMoveSignal.connect(mi.onMouseMove)
        rv.mouseReleaseSignal.connect(mi.onMouseRelease)
        rv.mouseWheelSignal.connect(mi.onMouseWheel)
        rv.mouseMoveSignal.connect(self._updateCamera)
        rv.mouseWheelSignal.connect(self._updateCamera)

    def disconnectInteractorSignals(self):
        if not self._mouseInteractor:
            print "Warning: No interactor set to disconnect signals!"
            return

        rv = self._ui._renderView
        mi = self._mouseInteractor
        rv.mousePressSignal.disconnect(mi.onMousePress)
        rv.mouseMoveSignal.disconnect(mi.onMouseMove)
        rv.mouseReleaseSignal.disconnect(mi.onMouseRelease)
        rv.mouseWheelSignal.disconnect(mi.onMouseWheel)
        rv.mouseMoveSignal.disconnect(self._updateCamera)
        rv.mouseWheelSignal.disconnect(self._updateCamera)

    def renderView(self):
        return self._ui._renderView

    def mouseInteractor(self):
        return self._mouseInteractor

    @property
    def compositor(self):
        return self.__compositor

    @QtCore.Slot('QColor')
    def setBackgroundColor(self, color):
        self.__compositor.set_background_color(color.getRgb())
        self._ui._renderView.setBackgroundBrush(QBrush(color))

    @QtCore.Slot(bool)
    def enableGeometryColor(self, enable):
        self.__compositor.enableGeometryColor(enable)

    @QtCore.Slot(bool)
    def enableLighting(self, enable):
        self.__compositor.enableLighting(enable)
