from PySide.QtGui import QColor, QPainter, QWidget
from PySide.QtCore import QRect


class QColorBar(QWidget):
    def __init__(self, parent=None):
        super(QColorBar, self).__init__(parent)
        self.__lut = None
        self.__x = None
        self._minMax = tuple([None, None])

    def resizeEvent(self, e):
        self.repaint()

    def setMinMax(self, minMax):
        self._minMax = minMax

    def setLookupTable(self, lookup_table):
        self.__lut = lookup_table.lut
        self.__x = lookup_table.x
        self.update()

    def paintEvent(self, e):
        painter = QPainter(self)

        if self.__lut is not None and self.__x is not None:

            colorCount = len(self.__lut)
            fullWidth = self.rect().width()
            height = self.rect().height() / 1

            xs = self.__x
            if xs[-1] == 1.0:
                # Need to make some space for the 1.0 color
                P = 0.1
                for i in range(0, len(xs)):
                    xs[i] = xs[i] - i * P / colorCount

            # Render the color bar
            for i in range(0, colorCount):
                # Render the color block
                x = xs[i]
                if i == colorCount - 1:
                    nextX = 1.0
                else:
                    nextX = xs[i + 1]
                blockWidth = fullWidth * (nextX - x)
                startX = fullWidth * x
                blockRect = QRect(startX, 0, startX + blockWidth, height)
                color = QColor(self.__lut[i][0],
                               self.__lut[i][1],
                               self.__lut[i][2])
                painter.fillRect(blockRect, color)
        else:
            painter.fillRect(self.rect(), QColor(236, 236, 236))
