## Creating a clean build machine for MacOS
This has been tested for MacOS versions:

- 10.12.4 Sierra
- 10.10.5 Yosemite

## install a clean version of the OS

These instructions assume that the OS is newly installed, and only minimal changes have been made to establish connectivity as needed (for example, proxies are set, if you're behind a firewall).

## install homebrew

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

## install packages

    brew install python
    brew install Homebrew/python/pillow
    brew install cmake
    brew tap cartr/qt4
    brew tap-pin cartr/qt4
    brew install qt
    brew install pyside
    brew install pyside-tools
    pip install numpy
    cd
    mkdir LANL
    mkdir LANL/git
    cd LANL/git
    git clone https://gitlab.kitware.com/cinema/qt-viewer.git
    git clone https://gitlab.kitware.com/cinema/cinema_python.git
    export PYTHONPATH=~/LANL/git/cinema_python

## test the viewer
    cd qt-viewer
    python Cinema.py

## create app and dmg
    cd qt-viewer
    make
